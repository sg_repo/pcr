#include <iostream>
#include <unistd.h>
#include "platform/pcrCycler.h"

using namespace std;

void pcrStatusCallBack(void* pParam)
{
	PCR_STATUS_INFO* pData = NULL;

	if(pParam == NULL)
	{
		cout<<"["<<__func__<<"]["<<__LINE__<<"]"<<"[Error]"<<endl;
		return;
	}

	pData = (PCR_STATUS_INFO*)pParam;
//	cout<<"["<<__func__<<"]["<<__LINE__<<"]"<<pData->eEvent<<endl;

	if(pData->eEvent == EVENT_INIT)
	{
		cout<<"["<<__func__<<"]["<<__LINE__<<"]"<<" EVENT INIT"<<endl;
		cout<<"["<<__func__<<"]["<<__LINE__<<"]"<<" Init Device"<<endl;
		cout<<"["<<__func__<<"]["<<__LINE__<<"]"<<pData->stProtoInfo[0].temperature[2]<<endl;
	}
	else if(pData->eEvent == EVENT_STATUS)
	{
//		cout<<"["<<__func__<<"]["<<__LINE__<<"]"<<" EVENT STATUS"<<endl;
/*
		cout<<"--------------------------------------"<<endl;
		cout<<"HOTLID 1 INFO"<<endl;
		cout<<"--------------------------------------"<<endl;
		cout<<"hId= "<<pData->stHotlidInfo[0].hId<<endl;
		cout<<"hotlidPresentTemperature= "<<pData->stHotlidInfo[0].hotlidPresentTemperature<<endl;
		cout<<"--------------------------------------"<<endl;
		cout<<"HOTLID 2 INFO"<<endl;
		cout<<"--------------------------------------"<<endl;
		cout<<"hId= "<<pData->stHotlidInfo[1].hId<<endl;
		cout<<"hotlidPresentTemperature= "<<pData->stHotlidInfo[1].hotlidPresentTemperature<<endl;
		cout<<"--------------------------------------"<<endl;
		cout<<endl;
		cout<<"--------------------------------------"<<endl;
		cout<<"PLATE SECTION 1"<<endl;
		cout<<"--------------------------------------"<<endl;
		cout<<"wId= "<<pData->stWellInfo[0].wId<<endl;
		cout<<"wellPresentTemperature= "<<pData->stWellInfo[0].wellPresentTemperature<<endl;
		cout<<"wellStep= "<<pData->stWellInfo[0].wellStep<<endl;
		cout<<"wellCycle= "<<pData->stWellInfo[0].wellCycle<<endl;
		cout<<"wellRemainTime= "<<pData->stWellInfo[0].wellRemainTime<<endl;
		cout<<"wellProgressTime= "<<pData->stWellInfo[0].wellProgressTime<<endl;
		cout<<"--------------------------------------"<<endl;
		cout<<"PLATE SECTION 2"<<endl;
		cout<<"--------------------------------------"<<endl;
		cout<<"wId= "<<pData->stWellInfo[1].wId<<endl;
		cout<<"wellPresentTemperature= "<<pData->stWellInfo[1].wellPresentTemperature<<endl;
		cout<<"wellStep= "<<pData->stWellInfo[1].wellStep<<endl;
		cout<<"wellCycle= "<<pData->stWellInfo[1].wellCycle<<endl;
		cout<<"wellRemainTime= "<<pData->stWellInfo[1].wellRemainTime<<endl;
		cout<<"wellProgressTime= "<<pData->stWellInfo[1].wellProgressTime<<endl;
		cout<<"--------------------------------------"<<endl;
		cout<<"PLATE SECTION 3"<<endl;
		cout<<"--------------------------------------"<<endl;
		cout<<"wId= "<<pData->stWellInfo[2].wId<<endl;
		cout<<"wellPresentTemperature= "<<pData->stWellInfo[2].wellPresentTemperature<<endl;
		cout<<"wellStep= "<<pData->stWellInfo[2].wellStep<<endl;
		cout<<"wellCycle= "<<pData->stWellInfo[2].wellCycle<<endl;
		cout<<"wellRemainTime= "<<pData->stWellInfo[2].wellRemainTime<<endl;
		cout<<"wellProgressTime= "<<pData->stWellInfo[2].wellProgressTime<<endl;
		cout<<"--------------------------------------"<<endl;
*/		
	}
}

void eventCallback(void *pParam)
{
	PCR_EVENT_INFO* pData = NULL;

	if(pParam == NULL)
	{
		cout<<"["<<__func__<<"]["<<__LINE__<<"]"<<"[Error]"<<endl;
		return;
	}

	pData = (PCR_EVENT_INFO*)pParam;
	cout<<"["<<__func__<<"]["<<__LINE__<<"]"<<pData->eEvent<<endl;
}

int main(int argc, char* argv[])
{
	pcrCycler *tmp = new pcrCycler;

	tmp->registerEventNotify(EVENT_INIT,pcrStatusCallBack);
	tmp->initialize();
	tmp->registerEventNotify(EVENT_STATUS,pcrStatusCallBack);
	tmp->registerEventNotify(EVENT_DRAWER_OPEN,eventCallback);
	tmp->registerEventNotify(EVENT_DRAWER_CLOSE,eventCallback);
	tmp->registerEventNotify(EVENT_PLATE1_COMPLETE,eventCallback);
	tmp->registerEventNotify(EVENT_PLATE2_COMPLETE,eventCallback);
	tmp->registerEventNotify(EVENT_PLATE3_COMPLETE,eventCallback);
	tmp->registerEventNotify(EVENT_CALIBRATION,eventCallback);
	tmp->registerEventNotify(EVENT_EXPORT,eventCallback);
	tmp->registerEventNotify(EVENT_IMPORT,eventCallback);
	tmp->registerEventNotify(EVENT_HOTLID_PREPARED,eventCallback);

	PCR_STATUS_INFO data = {};
	PCR_PLATE_INFO plateData = {};
	bool status=true;
	string proto;
	string name;
	string product;
	string resultPath;
	int count, i, ret;
	string namelist[100];
	string datalist[100];
	PCR_IMPORT_PROTOCOL_INFO importInfolist[100] = {};
	PCR_EXPORT_PROTOCOL_INFO exportInfolist[100] = {};
	PCR_EXPORT_PROTOCOL_INFO loadInfolist[100] = {};
	PCR_PLATE_SET pcrSetInfo = {};

	PCR_PROTOCOL_INFO rInfo = {};
	PCR_PROTOCOL_INFO wInfo = {};

	do{
		char keyInput;
		cout<<"b = sequece 3->n"<<endl;
		cout<<"a = pcrGetPlateInfo"<<endl;
		cout<<"d = pcrGetResultPath"<<endl;
		cout<<"n = run(PCR_START)"<<endl;
		cout<<"j = run(PCR_SUSPEND)"<<endl;
		cout<<"k = run(PCR_RESUME)"<<endl;
		cout<<"o = pcrStop()"<<endl;
		cout<<"h = checkHotlidConnection()"<<endl;
		cout<<"c = checkPeltierConnection()"<<endl;
		cout<<"e = editPcrProductName()"<<endl;
		cout<<"m = makePcrProductName()"<<endl;
		cout<<"u = deletePcrExperimentResultData()"<<endl;
		cout<<"l = loadPcrProtoNameList()"<<endl;
		cout<<"p = loadPcrProductList()"<<endl;
		cout<<"t = deletePcrProtocol()"<<endl;
		cout<<"w = writePcrProtocol()"<<endl;
		cout<<"r = loadPcrProtocol()"<<endl;
		cout<<"s = savePcrExperimentResultData()"<<endl;
		cout<<"g = selectPcrExperimentResultData()"<<endl;
		cout<<"i = loadPcrExperimentResultData()"<<endl;
		cout<<"v = pcrDeviceSwVer()"<<endl;
		cout<<"q = exitDevice()"<<endl;
		cout<<"z = exportShowUsbProtocol()"<<endl;
		cout<<"x = exportExecUsbProtocol()"<<endl;
		cout<<"1 = showUsbProtocol()"<<endl;
		cout<<"2 = importUsbProtocol()"<<endl;
		cout<<"3 = setPlateWellInfo()"<<endl;
		cout<<"4 = pcrGetStatus(....)"<<endl;
		cout<<"6 = callibration()"<<endl;
		cout<<"7 = setWellPlate(.....)"<<endl;
		cout<<"8 = powerOff()"<<endl;
		cout<<"9 = openDrawer()"<<endl;
		cout<<"0 = closeDrawer()"<<endl;
		cout<<"/ = testApi()"<<endl;
		cin>>keyInput;
		switch(keyInput){
			case 'b' :
					pcrSetInfo.experimentName = "20190610_1302";
					pcrSetInfo.plateType = PLATE_8TRIPS;
					pcrSetInfo.dyeSet[DYE_FAM] = 1;
					pcrSetInfo.dyeSet[DYE_HEX] = 1;
					pcrSetInfo.dyeSet[DYE_CALRED610] = 1;
					pcrSetInfo.wellType[0].used = 1;
					pcrSetInfo.wellType[0].type = WELL_NC;
					pcrSetInfo.wellType[1].used = 1;
					pcrSetInfo.wellType[1].type = WELL_PC;
					tmp->setPlateWellInfo(pcrSetInfo);

					cout<<"run sequence"<<endl;
					tmp->initialize();

					proto = "test";
					product = "seeplex";
					tmp->setWellPlate(product,proto);
					ret = tmp->pcrRun(PCR_START);

					cout<<"ret = "<<ret<<endl;
					break;
			case 'a' :
					tmp->pcrGetPlateInfo(&plateData);
					break;
			case 'd' :
					pcrSetInfo.experimentName = "20190610_1302";
					tmp->setPlateWellInfo(pcrSetInfo);

					tmp->pcrGetResultPath(resultPath, datalist,count);
					cout<<resultPath<<endl;
					cout<<count<<endl;
					for( i = 0; i<count; i++)
					{
						cout<<datalist[i]<<endl;
					}
					break;
			case 'n' :
					cout<<"run sequence"<<endl;
					tmp->initialize();

					proto = "test";
					product = "seeplex";
					tmp->setWellPlate(product,proto);
					
					ret = tmp->pcrRun(PCR_START);

					cout<<"ret = "<<ret<<endl;
					break;
			case 'j' :
					tmp->pcrRun(PCR_SUSPEND);
					break;
			case 'k' :
					tmp->pcrRun(PCR_RESUME);
					break;
			case 'o' :
					tmp->pcrStop();
					break;
			case 'h' :
					ret = tmp->checkHotlidConnection(HOTLID_SECTION_1);
					cout<<ret<<endl;
					ret = tmp->checkHotlidConnection(HOTLID_SECTION_2);
					cout<<ret<<endl;
					break;
			case 'c' :
					ret = tmp->checkPeltierConnection(PLATE_SECTION_1);
					cout<<ret<<endl;
					ret = tmp->checkPeltierConnection(PLATE_SECTION_2);
					cout<<ret<<endl;
					ret = tmp->checkPeltierConnection(PLATE_SECTION_3);
					cout<<ret<<endl;
					break;
			case 'm' :
					tmp->makePcrProductName("1plex");
					break;
			case 'e' :
					tmp->editPcrProductName("seeplex", "newplex");
					break;
			case 'u' :
					name = "20180510_1400";
					tmp->deletePcrExperimentResultData(name, loadInfolist, count);
					cout<<count<<endl;
					for( i = 0; i<count; i++)
					{
						cout<<loadInfolist[i].experimentName<<endl;
						cout<<loadInfolist[i].project<<endl;
						cout<<loadInfolist[i].protocolName<<endl;
						cout<<loadInfolist[i].totalRunTime<<endl;
						cout<<loadInfolist[i].createDate<<endl;
						cout<<loadInfolist[i].status<<endl;
					}
					break;
			case 'l' :
					product = "seeplex";
					tmp->loadPcrProtoNameList(product, namelist, count);
					cout<<count<<endl;
					for( i = 0; i<count; i++)
					{
						cout<<namelist[i]<<endl;
					}
					break;
			case 't' :
					product = "seeplex";
					name = "DB1";
					tmp->deletePcrProtocol(product, name);
					cout<<ret<<endl;
					break;
			case 'p' :
					tmp->loadPcrProductList(namelist, count);
					cout<<count<<endl;
					for( i = 0; i<count; i++)
					{
						cout<<namelist[i]<<endl;
					}
					break;
			case 'w' :
					product = "Custom";
					name = "DB1";

					wInfo.temperatureCount=5;
					wInfo.intervalCount=5;
					wInfo.groupCount=5;
					wInfo.repetitionCount=3;
					wInfo.captureCount=5;
					wInfo.meltStartTemperatureCount=5;
					wInfo.meltEndTemperatureCount=5;
					wInfo.temperature = new int[5]{94,94,60,72,72};
					wInfo.interval = new int[5]{900,30,90,90,600};
					wInfo.group = new int[5]{1,2,2,2,3};
					wInfo.repetition = new int[3]{1,40,1};
					wInfo.capture = new int[5]{0, 0, 0, 0, 0};
					wInfo.meltStartTemperature = new int[5]{30, 30, 30, 30, 30};
					wInfo.meltEndTemperature = new int[5]{80, 80, 80, 80, 80};
					wInfo.totalProcessTime=3*3600000;//3h
					wInfo.name = name;
					wInfo.product = product;

					tmp->savePcrProtocol(name, product, wInfo);

					product = "seeplex";
					name = "jsonProtocol";

					wInfo.name = name;
					wInfo.product = product;
					
					tmp->savePcrProtocol(name, product, wInfo);
					break;
			case 'r' :
					product = "seeplex";
					name = "jsonProtocol";
					tmp->loadPcrProtocol(name, product, rInfo);
					break;
			case 's' :
					tmp->savePcrExperimentResultData();
					break;
			case 'v' :
					tmp->pcrDeviceSwVer();
					break;
			case 'q' :
					cout<<"exit"<<endl;
					tmp->exitDevice();
					break;
			case 'x' :
					name = "20190610_1302"; 
					tmp->exportExecUsbProtocol(name);
					break;
			case 'g' :
					name = "20190610_1302"; 
					proto = "test";
					tmp->selectPcrExperimentResultData(name, proto, &plateData);
					break;
			case 'i' :
					tmp->loadPcrExperimentResultData(loadInfolist, count);
					cout<<count<<endl;
					for( i = 0; i<count; i++)
					{
						cout<<loadInfolist[i].experimentName<<endl;
						cout<<loadInfolist[i].project<<endl;
						cout<<loadInfolist[i].protocolName<<endl;
						cout<<loadInfolist[i].totalRunTime<<endl;
						cout<<loadInfolist[i].createDate<<endl;
						cout<<loadInfolist[i].status<<endl;
					}
					break;
			case 'z' :
					tmp->exportShowUsbProtocol(exportInfolist, count);
					cout<<count<<endl;
					for( i = 0; i<count; i++)
					{
						cout<<exportInfolist[i].experimentName<<endl;
						cout<<exportInfolist[i].project<<endl;
						cout<<exportInfolist[i].protocolName<<endl;
						cout<<exportInfolist[i].totalRunTime<<endl;
						cout<<exportInfolist[i].createDate<<endl;
						cout<<exportInfolist[i].status<<endl;
					}
					break;
			case '1' :
					tmp->importShowUsbProtocol(importInfolist,count);
					cout<<count<<endl;
					for( i = 0; i<count; i++)
					{
						cout<<importInfolist[i].name<<endl;
						cout<<importInfolist[i].stepCount<<endl;
						cout<<importInfolist[i].totalRunTime<<endl;
						cout<<importInfolist[i].createDate<<endl;
					}
					break;
			case '2' :
					product = "seeplex";
					name = "default";
					tmp->importExecUsbProtocol(product, name);
					break;
			case '3' :
					pcrSetInfo.experimentName = "20190610_1302";
					pcrSetInfo.plateType = PLATE_8TRIPS;
					pcrSetInfo.dyeSet[DYE_FAM] = 1;
					pcrSetInfo.dyeSet[DYE_HEX] = 1;
					pcrSetInfo.dyeSet[DYE_CALRED610] = 1;
					pcrSetInfo.wellType[0].used = 1;
					pcrSetInfo.wellType[0].type = WELL_NC;
					pcrSetInfo.wellType[1].used = 1;
					pcrSetInfo.wellType[1].type = WELL_PC;
					tmp->setPlateWellInfo(pcrSetInfo);
					break;
			case '4' :
					tmp->pcrGetStatus(&data);
					break;
			case '6' :
					tmp->calibration();
					break;
			case '7' :
					pcrSetInfo.experimentName = "20190610_1302";
					pcrSetInfo.plateType = PLATE_8TRIPS;
					pcrSetInfo.dyeSet[DYE_FAM] = 1;
					pcrSetInfo.dyeSet[DYE_HEX] = 1;
					pcrSetInfo.dyeSet[DYE_CALRED610] = 1;
					pcrSetInfo.dyeSet[DYE_QUASAR670] = 1;
					pcrSetInfo.dyeSet[DYE_QUASAR705] = 0;
					pcrSetInfo.wellType[0].used = 1;
					pcrSetInfo.wellType[0].type = WELL_NC;
					tmp->setPlateWellInfo(pcrSetInfo);

					proto = "default";
					product = "seeplex";
					tmp->setWellPlate(product,proto);
					break;
			case '8' :
					tmp->powerOff();
					break;
			case '9' :
					tmp->openDrawer();
					break;
			case '0' :
					tmp->closeDrawer();
					break;
			case '/' :
					pcrSetInfo.experimentName = "20190610_1302";
					pcrSetInfo.plateType = PLATE_8TRIPS;
					pcrSetInfo.dyeSet[DYE_FAM] = 1;
					pcrSetInfo.dyeSet[DYE_HEX] = 1;
					pcrSetInfo.dyeSet[DYE_CALRED610] = 1;
					pcrSetInfo.wellType[0].used = 1;
					pcrSetInfo.wellType[0].type = WELL_NC;
					pcrSetInfo.wellType[1].used = 1;
					pcrSetInfo.wellType[1].type = WELL_PC;
					tmp->setPlateWellInfo(pcrSetInfo);

					proto = "test";
					product = "seeplex";
					tmp->setWellPlate(product,proto);
//					cout<<"quant dummy"<<endl;

//					proto = "quantDummy";
//					product = "seeplex";
//					tmp->setWellPlate(product,proto);
					tmp->testApi(argv[2]);
					break;
		}

	}while(status);

	while(1)
	{
		sleep(5);
	}

	cout<<"--------------"<<endl;

	delete tmp;
	return 0;
}
