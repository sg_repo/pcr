ROOT_DIR?=$(shell pwd)

PLATFORM_DIR="platform"
###############################################################################
#LDCONFIG?=ldconfig

ifneq ($V,1)
Q ?= @
endif

DEBUG	= -g
#DEBUG	= -O2
CC	= g++
INCLUDE	= -I. -I$(ROOT_DIR)/AppLibs/local/include -I/usr/include/python3.5
CFLAGS	= $(DEBUG) -Wall $(INCLUDE) -Winline -pipe
LDFLAGS = -L$(ROOT_DIR)/AppLibs/local/lib -L/usr/lib -lpython3.5m
OPENCV_LIBS = -lopencv_core -lopencv_highgui -lopencv_imgproc -lopencv_imgcodecs
LIBS    = -lmcp3424 -lHerkuleX -lmightyzap -lwiringPi -lwiringPiPca9685 -lraspicam -lraspicam_cv -lpcrCycler
###############################################################################

CPP_SRC = appMain.cpp

HEADERS =	$(shell ls *.h)

OBJ	=	$(SRC:.c=.o)

CPP_OBJ = $(CPP_SRC:.cpp=.o)

.cpp.o:
	$(CC) -c $(CFLAGS) $<

all:		hwtest

.PHONY: hwtest
hwtest: $(CPP_OBJ)
	$Q echo "main App"
	$Q $(CC) -o $@ $(CPP_OBJ) $(CFLAGS) $(LDFLAGS) $(LIBS) $(OPENCV_LIBS)
	$Q rm -f $(CPP_OBJ)

.PHONY:	clean
clean:
	$Q echo "[Clean]"
	rm -f hwtest
	rm -f $(OBJ) $(CPP_OBJ)
