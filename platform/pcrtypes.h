#ifndef __PCR_TYPES_H__
#define __PCR_TYPES_H__

#include <string>

namespace pcr {

	enum
	{
		PLATE_SECTION_1 = 0,
		PLATE_SECTION_2 = 1,
		PLATE_SECTION_3 = 2,
		MAX_PLATE_SECTION_COUNT = 3,
		PCR_BASIC = 0,
		PCR_QUANT = 1,
		PCR_MELT = 2,
		PLATE_8TRIPS = 1,
		PLATE_96FILMS = 2,
		PLATE_96PLATE = 3,
		WELL_SAMPLE = 0,
		WELL_PC = 1,
		WELL_NC = 2,
		DYE_FAM = 0,
		DYE_HEX = 1,
		DYE_CALRED610 = 2,
		DYE_QUASAR670 = 3,
		DYE_QUASAR705 = 4,
		MAX_DYE_COUNT = 5,
		HOTLID_SECTION_1 = 0,
		HOTLID_SECTION_2 = 1,
		MAX_HOTLID_SECTION_COUNT = 2,
		INIT_STATUS = 1,
		SUCCESS_STATUS = 2,
		FAIL_STATUS = 3,
		PCR_START = 0,
		PCR_RESUME = 1,
		PCR_SUSPEND = 2,
		PCR_STOP = 3,
	};

	enum EVENT
	{
		EVENT_INIT,
		EVENT_STATUS,
		EVENT_DRAWER_OPEN,
		EVENT_DRAWER_CLOSE,
		EVENT_CALIBRATION,
		EVENT_EXPORT,
		EVENT_IMPORT,
		EVENT_HOTLID_PREPARED,
		EVENT_PLATE1_COMPLETE,
		EVENT_PLATE2_COMPLETE,
		EVENT_PLATE3_COMPLETE,
		EVENT_MAX
	};

	typedef struct
	{
		EVENT eEvent;
		int eventInfo;
	}PCR_EVENT_INFO;

	typedef struct
	{
		int temperatureCount;
		int *temperature;
		int intervalCount;
		int *interval;
		int groupCount;
		int *group;
		int repetitionCount;
		int *repetition;
		int captureCount;
		int *capture;
		int meltStartTemperatureCount;
		int *meltStartTemperature;
		int meltEndTemperatureCount;
		int *meltEndTemperature;
		int totalProcessTime;
		std::string name;
		std::string product;
	}PCR_PROTOCOL_INFO;

	typedef struct
	{
		int wId;
		int wellStep;
		int wellCycle;
		int wellRemainTime;
		int wellProgressTime;
		double wellPresentTemperature;
		double wellMeltTargetTemperature;
	}PCR_WELL_INFO;

	typedef struct
	{
		int hId;
		double hotlidPresentTemperature;
	}PCR_HOTLID_INFO;

	typedef struct
	{	
		bool init;
		EVENT eEvent;
		int type;
		int stProtoInfoCount;
		int stWellInfoCount;
		int stHotlidInfoCount;
		const PCR_PROTOCOL_INFO *stProtoInfo;
		PCR_WELL_INFO *stWellInfo;
		PCR_HOTLID_INFO *stHotlidInfo;
	}PCR_STATUS_INFO;

	typedef struct
	{
		int totalRunTime;
		int status;
		std::string experimentName;
		std::string project;
		std::string protocolName;
		std::string createDate;
	}PCR_EXPORT_PROTOCOL_INFO;

	typedef struct
	{
		int stepCount;
		int totalRunTime;
		int createDate;
		std::string name;
	}PCR_IMPORT_PROTOCOL_INFO;

	typedef struct
	{
		int pictureSet;
		int quantIndex;
		int meltIndex;
		int type;
	}PCR_STEP_MAP_INFO;

	typedef struct
	{
		int cycleCount;
		int step;
		int holeCount;
		int *captureCycle;
		int *quantCycle;
		double** well;
	}PCR_QUANT_RES;

	typedef struct
	{
		int meltTemperatureCount;
		int step;
		int holeCount;
		int *captureTemperature;
		double *meltTemperature;
		double** well;
	}PCR_MELT_RES;

	typedef struct
	{
		int used;
		int type;
	}PCR_WELL_TYPE;

	typedef struct
	{	
		int plateType;
		int dyeSet[MAX_DYE_COUNT];
		PCR_WELL_TYPE wellType[96];
		std::string experimentName;
	}PCR_PLATE_SET;


	typedef struct
	{	
		int status;
		int stage;
		int duration;
		int dyeCount;
		int stepMapCount;
		int quantStepCount;
		int meltStepCount;
		std::string experimentDate;
		std::string experimentName;
		std::string protocolName;
		std::string productName;
		std::string serialNumber;
		std::string temperatureLog[3];
		PCR_PLATE_SET plateSet;
		PCR_STEP_MAP_INFO* stepMap;
		PCR_QUANT_RES** quantResult;
		PCR_MELT_RES** meltResult;
	}PCR_PLATE_INFO;
}
#endif /*__PCR_H__*/
