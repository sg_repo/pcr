#ifndef __PCR_CYCLER_H__
#define __PCR_CYCLER_H__

#include <string>
#include <queue>
#include <mutex>
#include <condition_variable>
#include "pcrtypes.h"

using namespace pcr;
using namespace std;

namespace pcr {

	class pcrCycler {
	
	enum protocoInfo
	{
		INDEX0 = 0,
		INDEX1 = 1,
		INDEX2 = 2,
		MAX_PROTOCOL_NUM = 3
	};

	enum stage
	{
		CONTROL,
		STABILIZE,
		MAINTAIN,
		END
	};

	enum define
	{
		CE0 = 0,
		ON = 1,
		OFF = 0,
		
		SERIESRESISTOR = 18000,
		THERMISTORNOMINAL =100000,
		TEMPERATURENOMINAL = 25,
		NUMSAMPLES = 50,

		HOTLID1_CH = 6,
		HOTLID2_CH = 7,
		HOTLID1_IDX = 0,
		HOTLID2_IDX = 1,
		HOTLID_MAX_NUM = 2,

		PELTIER1_IDX = 0,
		PELTIER2_IDX = 1,
		PELTIER3_IDX = 2,
		PELTIER_MAX_NUM = 3,
		PELTIER1_EN_PIN = 30,
		PELTIER2_EN_PIN = 21,
		PELTIER3_EN_PIN = 22,
		PELTIER4_EN_PIN = 23,
		PELTIER5_EN_PIN = 24,
		PELTIER6_EN_PIN = 25,
		PELTIER1_SEC = 10,
		PELTIER2_SEC = 11,
		PELTIER3_SEC = 12,
		PELTIER1_CH = 0,
		PELTIER2_CH = 1,
		PELTIER3_CH = 2,
		PELTIER4_CH = 3,
		PELTIER5_CH = 4,
		PELTIER6_CH = 5,
		PELTIER_COOLING = 0,
		PELTIER_HITTING = 1,

		SERIAL_0 = 0,
		SERIAL_1 = 1,
		SERIAL_2 = 2,
		LINEAR_DIO_PIN = 26,
		
		CAM_1 = 0,
		CAM_2 = 1,
		CAM_3 = 2,
		CAM_4 = 3,
		CAM_5 = 4,
		CAM_6 = 5,
		CAM_NUM = 6,
		CAM_SELECT_PIN = 7,
		CAM_STACK1_EN1_PIN = 0,
		CAM_STACK1_EN2_PIN = 1,
		CAM_STACK2_EN1_PIN = 3,
		CAM_STACK2_EN2_PIN = 4,
	};

	enum pca9685_define
	{
		PCA9685_PIN_BASE0 = 300,
		PCA9685_PIN_BASE1 = 400,
		PCA9685_PIN_BASE2 = 500,
		PCA9685_MAX_PWM = 4096,
		PCA9685_HERTZ = 60,
		PCA9685_0_FAN1 = PCA9685_PIN_BASE0 + 0,
		PCA9685_0_FAN2 = PCA9685_PIN_BASE0 + 1,
		PCA9685_0_FAN3 = PCA9685_PIN_BASE0 + 2,
		PCA9685_1_LED_FAM_1 = PCA9685_PIN_BASE1 + 0,
		PCA9685_1_LED_HEX_1 = PCA9685_PIN_BASE1 + 1,
		PCA9685_1_LED_CALRED_1 = PCA9685_PIN_BASE1 + 2,
		PCA9685_1_LED_Q679_1 = PCA9685_PIN_BASE1 + 3,
		PCA9685_1_LED_Q705_1 = PCA9685_PIN_BASE1 + 4,
		PCA9685_1_LED_FAM_2 = PCA9685_PIN_BASE1 + 5,
		PCA9685_1_LED_HEX_2 = PCA9685_PIN_BASE1 + 6,
		PCA9685_1_LED_CALRED_2 = PCA9685_PIN_BASE1 + 7,
		PCA9685_1_LED_Q679_2 = PCA9685_PIN_BASE1 + 8,
		PCA9685_1_LED_Q705_2 = PCA9685_PIN_BASE1 + 9,
		PCA9685_1_LED_FAM_3 = PCA9685_PIN_BASE1 + 10,
		PCA9685_1_LED_HEX_3 = PCA9685_PIN_BASE1 + 11,
		PCA9685_1_LED_CALRED_3 = PCA9685_PIN_BASE1 + 12,
		PCA9685_1_LED_Q679_3 = PCA9685_PIN_BASE1 + 13,
		PCA9685_1_LED_Q705_3 = PCA9685_PIN_BASE1 + 14,
		PCA9685_2_PELTIER_PWM0 = PCA9685_PIN_BASE2 + 0,
		PCA9685_2_PELTIER_PWM1 = PCA9685_PIN_BASE2 + 1,
		PCA9685_2_PELTIER_PWM2 = PCA9685_PIN_BASE2 + 2,
		PCA9685_2_PELTIER_PWM3 = PCA9685_PIN_BASE2 + 3,
		PCA9685_2_PELTIER_PWM4 = PCA9685_PIN_BASE2 + 4,
		PCA9685_2_PELTIER_PWM5 = PCA9685_PIN_BASE2 + 5,
		PCA9685_2_HOTLID_PWM14 = PCA9685_PIN_BASE2 + 14,
		PCA9685_2_HOTLID_PWM15 = PCA9685_PIN_BASE2 + 15,
	};

	enum motor_define
	{
		DRAW_OPEN_STATUS = 0,
		DRAW_CLOSE_STATUS = 1,
		MES_MOTOR_ID1 = 0,
		MES_MOTOR_ID2 = 1,
		SOURCE_MOTOR_ID1 = 253,
		SOURCE_MOTOR_ID2 = 101,
		RECEIVE_MOTOR_ID1 = 110,
		RECEIVE_MOTOR_ID2 = 111,
		RECEIVE_MOTOR_ID3 = 112,
	};

	typedef struct 
	{
		long int msgType;
		char data[2048];
	}msg_queue_st;

	typedef struct
	{
		queue<double> x;
		queue<double> y;
	}point;

	typedef struct
	{
		double* xdata;
		double* ydata;
		double n;
		int xsize;
		int ysize;
	}fittingParam;

	public:

	typedef void (*PCR_CALLBACK)(void*);

	pcrCycler();
	~pcrCycler();

	void pcrGetSerialNum(string& pData);
	void pcrDeviceSwVer(void);
	void powerOff(void);
	void reboot(void);
	void initialize(void);
	void exitDevice(void);
	void pcrGetStatus(PCR_STATUS_INFO *pData);
	void pcrGetPlateInfo(PCR_PLATE_INFO *pData);
	int pcrGetResultPath(string& resultPath, string* dataList, int& dataCount);
	unsigned int getMillis(void);
	int openDrawer(void);
	int closeDrawer(void);
	int checkHotlidConnection(int ch);
	int checkPeltierConnection(int ch);
	int calibration(void);
	int registerEventNotify(EVENT eEvent, PCR_CALLBACK pPcrCallback);
	int preSetExperimentData(string product, string protName);
	int setWellPlate(string product, string protName);
	int setWellPlate(int sectionId, string product, string protName);
	int setPlateWellInfo(PCR_PLATE_SET& setInfo);
	int importShowUsbProtocol(PCR_IMPORT_PROTOCOL_INFO* infoList, int& count);
	int importExecUsbProtocol(string pcrProduct, string usbProtoName);
	int exportShowUsbProtocol(PCR_EXPORT_PROTOCOL_INFO* infoList, int& count);
	int exportExecUsbProtocol(string pcrExperimentName);
	int deletePcrProtocol(string product,string protoName);
	int loadPcrProtocol(string protoName, string product, PCR_PROTOCOL_INFO& outProtoInfo);
	int savePcrProtocol(string protoName, string product, PCR_PROTOCOL_INFO& protoInfo);
	int loadPcrProtoNameList(string product, string* nameList, int& nameCount);
	int loadPcrProductList(string* nameList, int& nameCount);
	int editPcrProductName(string source, string dest);
	int makePcrProductName(string product);
	int selectPcrExperimentResultData(string inExperiment, string inProtocol, PCR_PLATE_INFO *outData);
	int deletePcrExperimentResultData(string inExperiment, PCR_EXPORT_PROTOCOL_INFO* infoList, int& count);
	int loadPcrExperimentResultData(PCR_EXPORT_PROTOCOL_INFO* infoList, int& count);
	int savePcrExperimentResultData(void);
	int pcrRun(int status);
	int pcrStop(void);
	int dataRfuResult(void);
	void testApi(char* arg);

	private:
	int deviceInit(void);
	void deviceOff(void);
	double getMeanTemp(int ch);
	double getTemp(double temp);
	int adcRawRead(uint32_t channel);
	double round(double value, int pos);
	double constrain(double value, double minValue, double maxValue);
	double sum(const double x_data[], const int x_size[1]);
	double fitFunction(double x, double a, double b, double c);
	void power(const double a_data[], const int a_size[1], double y_data[], int y_size[1]);
	void curveFitting(const fittingParam* data, double *coeff1, double *coeff2, double *coeff3);
	void ledSetAll(int status);
	void ledSet(int idx, int value);
	void fanSet(int idx, int value);
	void hotlidSet(int idx, double value);
	void peltierSet(int idx, double value1, double value2, int& status);
	void peltierSet(int idx, int onOff);
	bool checkFileExist(const string& name);
	void controlTemperature(void);
	void controlHotlid(bool loop);
	void prepareHotlidTemperature(void);
	void protocolInitialize(void);
	void dataMapInitialize(void);
	void eventCallbackFire(PCR_EVENT_INFO *pData);
	void pcrCallbackFire(PCR_STATUS_INFO *pData);
	int callParameter(void);
	int calibrationInfoRecord(void);
	int callRecord(int idx);
	void controlCycler(void);
	void moveAngle(int id, int angle, int time, int led);
	void drawControl(int status);
	void mes(int prime, int sub);
	void messageManager(void);
	void setCaptureStatus(bool status);
	int serialInit(int port);
	int serialOpen(int port);
	int toRemoteCamera(int camIdx, int dye, string stage, int step, string item);
	int receiveFromRemoteCamera(int camIdx, int dye, string stage, int step, string item);
	int LastToRemoteCamera(void);
	int camera(void);
	int cameraReset(void);
	int cameraCapture(int index, string fileName);
	int cameraImageToRfuData(int index, int dye, string fileName);
	int totalCapture(void);

	PCR_CALLBACK pEventCallback[EVENT_MAX] = {};

	int serialFd = 0;
	int serialFd1 = 0;
	double parameter[PELTIER_MAX_NUM][3] = {};
	
	bool camCheck = false;
	bool drawerSet = false;
	bool checkCameraCapture[MAX_PLATE_SECTION_COUNT] = {};
	bool wellSetStatus[MAX_PLATE_SECTION_COUNT] = {};
	bool wellMeltStatus[MAX_PLATE_SECTION_COUNT] = {false,false,false};
	bool calibSetStatus = false;
	bool pcrSuspend = false;
	bool pcrReady = false;
	bool pcrPrepare = false;
	bool runPcrDev = true;

	mutex readyMutex;
	mutex prepareMutex;
	mutex i2cMutex;
	condition_variable readyCondVar;
	condition_variable prepareCondVar;
	pthread_t wThread;

	mutex cbStatusEventMutex;
	mutex cbGeneralEventMutex;
	mutex statusMutex;

	PCR_PROTOCOL_INFO protocolInfo[MAX_PROTOCOL_NUM] = {};
	PCR_STATUS_INFO pcrStatus = {};
	PCR_PLATE_SET pcrSetInfo = {};
	PCR_PLATE_INFO pcrPlateInfo = {};
	PCR_PLATE_SET pcrExperimentSetResult = {};
	PCR_PLATE_INFO pcrExperimentResult = {};

	PCR_WELL_INFO wellInfo[PELTIER_MAX_NUM] = {};
	PCR_HOTLID_INFO hotlidInfo[HOTLID_MAX_NUM] = {};

	string ttyUsbDev;
	string ttyUsbDev1;
	const string dirfileDB = "FileDB";
	const string dirProtocolDB = "protocolFileDB";
	const string dirExperimentDB = "experimentFileDB";
	const string dirSubExperimentDB = "RFU";
	const string dirProtocol = dirfileDB + "/" + dirProtocolDB;
	const string dirExperiment = dirfileDB + "/" + dirExperimentDB;
	const string dirSubExperiment = dirfileDB + "/" + dirExperimentDB + "/" + dirSubExperimentDB;
	}; //class
}

#endif /*__PCR_H__*/
